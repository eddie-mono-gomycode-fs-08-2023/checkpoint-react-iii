
import './App.css';
import Players from './Players';

import PlayersList from './PlayersList';

function App() {
  return (
    <div className="App">
      <div style={{ display: 'flex', alignContent: 'center', fontWeight: 'bold', flexWrap:'wrap' }}>
        <PlayersList />
      </div>
    </div>
  );
}

export default App;

import React from 'react';
import Player from './Player';
import Players from './Players';

const PlayersList = () => {
  return (
    <>
      {Players.map(el => <Player name={el.name} team={el.team} nationality={el.nationality} jerseyNumber={el.jerseyNumber} age={el.age} image={el.image}/>)}
    </>
  );
}

export default PlayersList;

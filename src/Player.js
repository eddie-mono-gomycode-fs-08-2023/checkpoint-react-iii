import React from 'react';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';

const Player = ({ name, team, nationality, jerseyNumber, age, image }) => {
  return (

    <Card style={{ display: 'flex', width:'25%'}}>
      <ListGroup variant="flush">
        <img src={image} style={{height: '300px'}} />
        <ListGroup.Item style={{ backgroundColor: 'blue'}}>Name: {name}</ListGroup.Item>
        <ListGroup.Item style={{ backgroundColor: 'red' }}>Team: {team}</ListGroup.Item>
        <ListGroup.Item style={{ backgroundColor: 'yellow' }}>Nationality: {nationality}</ListGroup.Item>
        <ListGroup.Item style={{ backgroundColor: 'green' }}>Jersey Number: {jerseyNumber}</ListGroup.Item>
        <ListGroup.Item style={{ backgroundColor: 'gray' }}>Age: {age}</ListGroup.Item>
      </ListGroup>
    </Card>

  );
}

export default Player;

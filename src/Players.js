  const playerList = [
    {
      name: 'Ronaldo', 
      team: 'Real Madrid',
      nationality: 'Portuguese',
      jerseyNumber: '7',
      age: '28',     
      image: 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.Xt7qozEdnJg2QHtAKbA-VwHaFj%26pid%3DApi&f=1&ipt=6263749204675b52ab47249e985915d66a2acbc2dd72cbb54f7b6158a6c3404d&ipo=images'
      },
  
    {
      name: 'Harzard',
      team: 'Chelsea',
      nationality: 'England',
      jerseyNumber: '88',
      age: '36',
      image: 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid%3DOIP.l2oyr0Zk7xV7wIu2bgW0yAHaEK%26pid%3DApi&f=1&ipt=0d74fbe6508af810aacde9ddd3350e40a61cf50fb016f9db10e78662e831f23e&ipo=images'
    },
  
    {
      name: 'Harzard',
      team: 'Chelsea',
      nationality: 'England',
      jerseyNumber: '25',
      age: '36',
      image: 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid%3DOIP.26dao5x4RzoDgXAYtxOX6QHaE8%26pid%3DApi&f=1&ipt=0381183c9810cdbb72ff544a3b0b813e4a2850b237133e2926ac9c88ac2905e4&ipo=images'
    },
  
    {
      name: 'Harzard',
      team: 'Chelsea',
      nationality: 'England',
      jerseyNumber: '9',
      age: '36',
      image: 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.mRRMVpTA9atkLvSCGs-kngHaEK%26pid%3DApi&f=1&ipt=dccc376195c2f21829f2632a8dd3e6f3e1701b807038c824fd071ebd133d11db&ipo=images'
    },
    ];
        

export default playerList;
